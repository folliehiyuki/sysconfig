# Sysconfig

This is an Ansible playbook to deploy my system configurations for desktop usage.

## 🧰 Usage

- Have a fresh installation of Alpine (after running `setup-alpine` and reboot)

- Install `ansible-core` and `git`

- Clone this repository

- Install needed external modules (e.g. `apk`, `pamd`, `mount`):

  ```bash
  ansible-galaxy install -r requirements/collections.yml
  ```

- Create an encrypted file to store your user password:

  ```bash
  mkdir -p host_vars/YOUR_HOSTNAME
  touch host_vars/YOUR_HOSTNAME/secrets.yml
  ansible-vault encrypt host_vars/YOUR_HOSTNAME/secrets.yml
  ansible-vault edit host_vars/YOUR_HOSTNAME/secrets.yml
  ```
  The file should look like this: `vault_password: <strong_&_secure_password>`

- Customize the variables in [group_vars/all.yml](./group_vars/all.yml).

  Note that some variables only accept a list of defined values. They are listed in [requirements/accepted_variables.yml](./requirements/accepted_variables.yml).

- Run the playbook:

  ```bash
  ansible-playbook setup.yml
  ```

- Reboot and login as the newly created normal user

- Proceed with [dotfiles-ansible](/folliehiyuki/dotfiles-ansible) playbook

## 🖊️ Notes

- This playbook assumes that the person running it is me 😃 and targets a single-user AlpineLinux system. It might do specific tasks that you don't like. Use with your own risks.

- The playbook assumes it's only run once. As such it doesn't take into account conflicted services when switching options in later runs.

- The playbook is intended to be run as **root**. Hence, it is separated from [dotfiles-ansible](/folliehiyuki/dotfiles-ansible), which should only be run as a normal user.

## 📄 License

MIT
