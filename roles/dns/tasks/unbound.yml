---
- name: unbound | Install dns-root-hints
  community.general.apk:
    name: dns-root-hints
    state: present

- name: unbound | Create /dev directory inside unbound chroot
  file:
    path: /etc/unbound/dev
    state: directory
    owner: root
    group: root
    mode: '755'

# We are operating on special devices
# copy module doesn't seem to work
- name: unbound | Copy needed devices to unbound chroot
  command:
    cmd: /bin/cp -a /dev/urandom /dev/null /etc/unbound/dev/
    creates: /etc/unbound/dev/urandom

# unbound user needs write permission to the anchor file, and also for the
# directory it is in (to create a temporary file)
- name: unbound | Create var directory inside unbound chroot
  file:
    path: /etc/unbound/var
    state: directory
    owner: root
    group: unbound
    mode: '775'

# unbound-anchor gives errors about connecting to IPv6 servers
- name: unbound | Generate trusted anchor file
  command:
    cmd: /usr/sbin/unbound-anchor -a /etc/unbound/var/trusted-key.key
    creates: /etc/unbound/var/trusted-key.key
  failed_when: false

- name: unbound | Ensure proper permissions on trusted anchor file
  file:
    path: /etc/unbound/var/trusted-key.key
    state: file
    owner: unbound
    group: unbound
    mode: '640'

- name: unbound | Gather package facts
  package_facts:
    manager: 'apk'

# We only need to run the DNS root hints update the 1st time, as
# dns-root-hints package comes with a monthly crontab
- name: unbound | Check the current DNS root zone version
  lineinfile:
    backup: true
    path: /usr/share/dns-root-hints/named.root
    regexp: '^;[ \t]+related version of root zone:[ \t]+{{ ansible_facts.packages["dns-root-hints"][0].version }}$'
    state: absent
  check_mode: true
  register: root_hints_check
  changed_when: false

- name: unbound | Get the latest DNS root hints
  command:
    cmd: /usr/bin/update-dns-root-hints
    removes: /usr/bin/update-dns-root-hints
  when: root_hints_check.found == 1

- name: unbound | Copy the updated root hints file to unbound chroot
  copy:
    src: /usr/share/dns-root-hints/named.root
    dest: /etc/unbound/var/named.root
    owner: root
    group: unbound
    mode: '640'

- name: unbound | Let dns-root-hints cron job copy root hints to unbound chroot
  blockinfile:
    path: /etc/periodic/monthly/dns-root-hints
    block: |
      updated_file="/usr/share/dns-root-hints/named.root"
      current_file="/etc/unbound/var/named.root"
      drh_new_ver=$(/bin/grep "related version of root zone:" $updated_file | /bin/grep -E -o '[0-9]{10}')
      drh_current_ver=$(/bin/grep "related version of root zone:" $current_file | /bin/grep -E -o '[0-9]{10}')

      if [ "$drh_new_ver" != "$drh_current_ver" ]; then
          /bin/cp -f $updated_file $current_file
          /bin/chown root:unbound $current_file
          /bin/chmod 0640 $current_file
      fi
    marker: '# {mark} COPY THE UPDATED ROOT HINTS TO UNBOUND CHROOT'
    insertafter: 'EOF'
    owner: root
    group: root
    mode: '755'

- name: unbound | Copy unbound config
  template:
    src: unbound.j2
    dest: /etc/unbound/unbound.conf
    owner: root
    group: root
    mode: '644'
    validate: /usr/sbin/unbound-checkconf %s

- name: unbound | Add unbound service to runlevel 'default'
  service:
    name: unbound
    runlevel: default
    enabled: true
    state: started
